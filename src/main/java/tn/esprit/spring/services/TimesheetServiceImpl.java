package tn.esprit.spring.services;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.esprit.spring.entities.Departement;
import tn.esprit.spring.entities.Employe;
import tn.esprit.spring.entities.Mission;
import tn.esprit.spring.entities.Role;
import tn.esprit.spring.entities.Timesheet;
import tn.esprit.spring.entities.TimesheetPK;
import tn.esprit.spring.repository.DepartementRepository;
import tn.esprit.spring.repository.EmployeRepository;
import tn.esprit.spring.repository.MissionRepository;
import tn.esprit.spring.repository.TimesheetRepository;

@Service
public class TimesheetServiceImpl implements ITimesheetService {

	@Autowired
	MissionRepository missionRepository;
	@Autowired
	DepartementRepository deptRepository;
	@Autowired
	TimesheetRepository timesheetRepository;
	@Autowired
	EmployeRepository employeRepository;

	private static final Logger l = LogManager.getLogger(TimesheetServiceImpl.class);
	private static final String MISSION_ID = "Mission ID : %d";
	private static final String EMPLOYE_ID = "Employe ID : %d";

	public int ajouterMission(Mission mission) {
		l.info("Adding mission");
		missionRepository.save(mission);
		l.debug("Mission added with id : %d", mission.getId());
		return mission.getId();
	}

	public void affecterMissionADepartement(int missionId, int depId) {
		l.info("Mission assigment");
		l.debug(MISSION_ID, missionId);
		l.debug("Department ID : %d", depId);

		Optional<Mission> findMission = missionRepository.findById(missionId);
		if (findMission.isPresent()) {
			Mission mission = findMission.get();

			Optional<Departement> findDept = deptRepository.findById(depId);
			if (findDept.isPresent()) {
				Departement dep = findDept.get();

				l.info("Mission and department found, assigning...");
				mission.setDepartement(dep);
				missionRepository.save(mission);
			} else {
				l.warn("Cannot find department with ID : %d", depId);
			}
		} else {
			l.warn("Cannot find mission with ID : %d", missionId);
		}
	}

	public void ajouterTimesheet(int missionId, int employeId, Date dateDebut, Date dateFin) {
		l.info("Adding timesheet");
		l.debug(MISSION_ID, missionId);
		l.debug(EMPLOYE_ID, employeId);
		l.debug("Start date: %tD", dateDebut);
		l.debug("End date : %tD", dateFin);

		TimesheetPK timesheetPK = new TimesheetPK();
		timesheetPK.setDateDebut(dateDebut);
		timesheetPK.setDateFin(dateFin);
		timesheetPK.setIdEmploye(employeId);
		timesheetPK.setIdMission(missionId);

		Timesheet timesheet = new Timesheet();
		timesheet.setTimesheetPK(timesheetPK);
		timesheet.setValide(false); // par defaut non valide
		timesheetRepository.save(timesheet);

	}

	public void validerTimesheet(int missionId, int employeId, Date dateDebut, Date dateFin, int validateurId) {
		l.info("Validate timesheet");
		l.debug(MISSION_ID, missionId);
		l.debug(EMPLOYE_ID, employeId);
		l.debug("Start date: %tD", dateDebut);
		l.debug("End date : %tD", dateFin);
		l.debug("Validator ID : %d", validateurId);
		Optional<Employe> findEmploye = employeRepository.findById(validateurId);
		if (findEmploye.isPresent()) {
			Employe validateur = findEmploye.get();

			Optional<Mission> findMission = missionRepository.findById(missionId);
			if (findMission.isPresent()) {
				Mission mission = findMission.get();

				// verifier s'il est un chef de departement (interet des enum)
				if (!validateur.getRole().equals(Role.CHEF_DEPARTEMENT)) {
					l.warn("Employe must be a department chief to validate timesheet");
					return;
				}
				l.info("Employe is a department chief");
				
				// verifier s'il est le chef de departement de la mission en question
				l.info("Check if employe is chief of the mission's department");
				l.debug("Mission's department ID : %d", mission.getDepartement().getId());
				boolean chefDeLaMission = false;
				for (Departement dep : validateur.getDepartements()) {
					l.debug("Department ID : %d", dep.getId());
					if (dep.getId() == mission.getDepartement().getId()) {
						l.info("Employe is the chief of the mission's department");
						chefDeLaMission = true;
						break;
					}
				}
				if (!chefDeLaMission) {
					l.warn("Employe must be chief of the mission's department");
					return;
				}
				//
				TimesheetPK timesheetPK = new TimesheetPK(missionId, employeId, dateDebut, dateFin);
				Timesheet timesheet = timesheetRepository.findBytimesheetPK(timesheetPK);
				timesheet.setValide(true);

				// Comment Lire une date de la base de données
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
				String startDate = dateFormat.format(timesheet.getTimesheetPK().getDateDebut());
				l.debug("Start date : %s", startDate);
			} else {
				l.warn("Cannot found mission with ID : %d", missionId);
			}
		} else {
			l.warn("Cannot found validator with ID : %d", validateurId);
		}

	}

	public List<Mission> findAllMissionByEmployeJPQL(int employeId) {
		l.info("Searching for mission by employe");
		l.debug(EMPLOYE_ID, employeId);
		return timesheetRepository.findAllMissionByEmployeJPQL(employeId);
	}

	public List<Employe> getAllEmployeByMission(int missionId) {
		l.info("Get all employe by mission");
		l.debug(MISSION_ID, missionId);
		return timesheetRepository.getAllEmployeByMission(missionId);
	}

}
