package tn.esprit.spring.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import tn.esprit.spring.entities.Departement;
import tn.esprit.spring.entities.Entreprise;
import tn.esprit.spring.repository.DepartementRepository;
import tn.esprit.spring.repository.EntrepriseRepository;

@Service
public class EntrepriseServiceImpl implements IEntrepriseService {

	@Autowired
    EntrepriseRepository entrepriseRepoistory;
	@Autowired
	DepartementRepository deptRepoistory;
	

private static final Logger l = LogManager.getLogger(TimesheetServiceImpl.class);
private static final String ENTREPRISE_ID = "Entreprise ID : %d";
private static final String DEPARTEMENT_ID = "DEPARTEMENT ID : %d";
	
	public int ajouterEntreprise(Entreprise entreprise) {
		l.info("Adding Entreprise");
		entrepriseRepoistory.save(entreprise);
		l.debug("Entreprise added with id : %d", entreprise.getId());
		return entreprise.getId();
	}

	public int ajouterDepartement(Departement dep) {
		l.info("Adding Departement");
		deptRepoistory.save(dep);
		l.debug("Departement added with id : %d", dep.getId());
		return dep.getId();
	}
	
	public void affecterDepartementAEntreprise(int depId, int entrepriseId) {
		//Le bout Master de cette relation N:1 est departement  
				//donc il faut rajouter l'entreprise a departement 
				// ==> c'est l'objet departement(le master) qui va mettre a jour l'association
				//Rappel : la classe qui contient mappedBy represente le bout Slave
				//Rappel : Dans une relation oneToMany le mappedBy doit etre du cote one.
		        l.info("Finding Entreprise by ID");	
		        l.debug(ENTREPRISE_ID, entrepriseId);
				Entreprise entrepriseManagedEntity = entrepriseRepoistory.findById(entrepriseId).get();
				l.info("Finding Department by ID");	
		        l.debug(DEPARTEMENT_ID,  depId);
				Departement depManagedEntity = deptRepoistory.findById(depId).get();
				l.info("departement selected");
				l.info("Affecting Entreprise to department");
				depManagedEntity.setEntreprise(entrepriseManagedEntity);
				l.info("Saving changes");
				deptRepoistory.save(depManagedEntity);
		
	}
	
	public List<String> getAllDepartementsNamesByEntreprise(int entrepriseId) {
		l.info("Searching for entreprise");
		l.debug(ENTREPRISE_ID, entrepriseId);
		Entreprise entrepriseManagedEntity = entrepriseRepoistory.findById(entrepriseId).get();
		l.info("Assigning departement names found by entreprise");
		List<String> depNames = new ArrayList<>();
		for(Departement dep : entrepriseManagedEntity.getDepartements()){
			l.debug(ENTREPRISE_ID, entrepriseId);
			l.debug(DEPARTEMENT_ID, dep.getId());
			depNames.add(dep.getName());
		}
		l.info("found departements");
		return depNames;
	}

	@Transactional
	public void deleteEntrepriseById(int entrepriseId) {
		l.info("Deleting Entreprise");
		l.debug(ENTREPRISE_ID, entrepriseId);
		entrepriseRepoistory.delete(entrepriseRepoistory.findById(entrepriseId).get());	
	}

	@Transactional
	public void deleteDepartementById(int depId) {
		l.info("Deleting Departement");
		l.debug(DEPARTEMENT_ID, depId);
		deptRepoistory.delete(deptRepoistory.findById(depId).get());	
	}


	public Entreprise getEntrepriseById(int entrepriseId) {
		l.info("Searching for Entreprise");
		l.debug(ENTREPRISE_ID, entrepriseId);
		return entrepriseRepoistory.findById(entrepriseId).get();	
	}

}
