package tn.esprit.spring;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import tn.esprit.spring.entities.Employe;
import tn.esprit.spring.entities.Mission;
import tn.esprit.spring.entities.Timesheet;
import tn.esprit.spring.repository.EmployeRepository;
import tn.esprit.spring.repository.MissionRepository;
import tn.esprit.spring.repository.TimesheetRepository;
import tn.esprit.spring.services.ITimesheetService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TimesheetServiceImplTest {

	@Autowired
	ITimesheetService tms;
	@Autowired
	MissionRepository missionRepository;
	@Autowired
	TimesheetRepository timesheetRepository;
	@Autowired
	EmployeRepository employeRepository;

	@Test
	public void testAddMission() {
		Mission mission = new Mission("NameTest", "DescriptionTest");
		int missionAddedId = tms.ajouterMission(mission);
		assertNotNull(missionRepository.findById(missionAddedId));
	}

	@Test
	public void testAssignMissionToDep() {
		int missionId = 1;
		int depId = 1;
		Optional<Mission> findMission = missionRepository.findById(missionId);
		if (findMission.isPresent()) {
			Mission mission = findMission.get();

			tms.affecterMissionADepartement(missionId, depId);
			assertEquals(mission.getDepartement().getId(), depId);
		} else {
			fail("Mission with id " + missionId + " not found");
		}
	}

	@Test
	public void testAddTimesheet() {
		int missionId = 1;
		int employeId = 1;
		Date dateDebut = new Date(160457832);
		Date dateFin = new Date(1604664722);

		int employeMissionNb = timesheetRepository.findAllMissionByEmployeJPQL(employeId).size() + 1;
		
		tms.ajouterTimesheet(missionId, employeId, dateDebut, dateFin);
		assertEquals(employeMissionNb, timesheetRepository.findAllMissionByEmployeJPQL(employeId).size());
	}

	@Test
	public void testValiderTimesheet() {
		int missionId = 1;
		int employeId = 1;
		Date dateDebut = new Date(1604578322);
		Date dateFin = new Date(1604664722);
		int validateurId = 1;
		
		tms.validerTimesheet(missionId, employeId, dateDebut, dateFin, validateurId);
		
		/*Optional<Mission> findMission = missionRepository.findById(missionId);
		if (findMission.isPresent()) {
			Mission mission = findMission.get();
			
			Optional<Employe> findEmploy = employeRepository.findById(employeId);
			if (findEmploy.isPresent()) {
				Employe employe = findEmploy.get();
				
				tms.validerTimesheet(missionId, employeId, dateDebut, dateFin, validateurId);
				
//				Timesheet timesheet = timesheetRepository.getTimesheetsByMissionAndDate(employe, mission, dateDebut, dateFin).get(0);
				assertEquals(timesheet.isValide(), true);
			} else {
				fail("Employe with id "+employeId+" not found");
			}
		} else {
			fail("Mission with id "+missionId+" not found");
		}*/
		
	}

	/*
	 * @Test public void testFindAllMissionByEmploye() { int employeId = 1;
	 * tms.findAllMissionByEmployeJPQL(employeId); }
	 * 
	 * @Test public void testGetAllEmployeByMission() { int missionId = 1;
	 * tms.getAllEmployeByMission(missionId); }
	 */
}
